const std = @import("std");
const builtin = @import("builtin");

const sdl = @cImport(@cInclude("SDL2/SDL.h"));

pub fn main() !void {
    // Initialize window creation
    if (sdl.SDL_Init(sdl.SDL_INIT_VIDEO | sdl.SDL_INIT_AUDIO) != 0) {
        @panic("SDL_Init Error!");
    }
    defer sdl.SDL_Quit();

    const win_width = 640;
    const win_height = 400;

    var window: ?*sdl.SDL_Window = sdl.SDL_CreateWindow(
        "zPong",
        sdl.SDL_WINDOWPOS_CENTERED,
        sdl.SDL_WINDOWPOS_CENTERED,
        win_width,
        win_height,
        sdl.SDL_WINDOW_SHOWN,
    );
    defer sdl.SDL_DestroyWindow(window);
    if (window == null) {
        @panic("SDL_CreateWindow Error!");
    }

    var renderer: ?*sdl.SDL_Renderer = sdl.SDL_CreateRenderer(
        window,
        -1,
        sdl.SDL_RENDERER_PRESENTVSYNC | sdl.SDL_RENDERER_ACCELERATED,
    );
    if (renderer == null) {
        @panic("SDL_CreateRenderer Error!");
    }
    defer sdl.SDL_DestroyRenderer(renderer);

    // Init game
    const ball_size = 10;
    var ball = sdl.SDL_Rect{
        .x = (win_width / 2) - (ball_size / 2),
        .y = (win_height / 2) - (ball_size / 2),
        .w = ball_size,
        .h = ball_size,
    };

    var ball_speed_x: c_int = 7;
    var ball_speed_y: c_int = 7;

    const paddle_width = 5;
    const paddle_height = 90;

    var player = sdl.SDL_Rect{
        .x = (win_width - 20),
        .y = (win_height / 2) - (paddle_height / 2),
        .w = paddle_width,
        .h = paddle_height,
    };
    var player_speed: c_int = 0;

    var opponent = sdl.SDL_Rect{
        .x = 10,
        .y = (win_height / 2) - (paddle_height / 2),
        .w = paddle_width,
        .h = paddle_height,
    };
    var opponent_speed: c_int = 7;

    var event: sdl.SDL_Event = undefined;
    var quit: bool = false;
    var served: bool = false;

    while (!quit) {
        // input
        while (sdl.SDL_PollEvent(&event) != 0) {
            switch (event.type) {
                else => {},

                sdl.SDL_QUIT => {
                    quit = true;
                },

                sdl.SDL_KEYDOWN => {
                    switch (event.key.keysym.sym) {
                        else => {},

                        sdl.SDLK_SPACE => {
                            served = true;
                            ball_speed_x *= if (std.crypto.random.boolean()) -1 else 1;
                            ball_speed_y *= if (std.crypto.random.boolean()) -1 else 1;
                        },

                        sdl.SDLK_UP => {
                            player_speed = -5;
                        },

                        sdl.SDLK_DOWN => {
                            player_speed = 5;
                        },
                    }
                },

                sdl.SDL_KEYUP => {
                    switch (event.key.keysym.sym) {
                        else => {},

                        sdl.SDLK_UP => {
                            player_speed = 0;
                        },

                        sdl.SDLK_DOWN => {
                            player_speed = 0;
                        },
                    }
                },
            }
        }

        // update

        // ball
        if (!served) {} else {
            ball.x += ball_speed_x;
            ball.y += ball_speed_y;
        }

        // ball collides with wall
        if ((ball.y < 0) or (ball.y + (ball_size / 2) > win_height)) {
            ball_speed_y *= -1;
        }

        if ((ball.x < 0) or (ball.x + (ball_size / 2) > win_width)) {
            // ball_speed_x *= -1;
            ball.x = (win_width / 2) - (ball_size / 2);
            ball.y = (win_height / 2) - (ball_size / 2);
            // served = false;

            ball_speed_x *= if (std.crypto.random.boolean()) -1 else 1;
            ball_speed_y *= if (std.crypto.random.boolean()) -1 else 1;
        }

        // ball collides with paddles
        if ((sdl.SDL_HasIntersection(&ball, &player) != 0) or (sdl.SDL_HasIntersection(&ball, &opponent) != 0)) {
            ball_speed_x *= -1;
        }

        // player
        player.y += player_speed;

        // checks if player going out of bounds
        if (player.y < 0) {
            player.y = 0;
        }
        if (player.y + paddle_height > win_height) {
            player.y = win_height - paddle_height;
        }

        // opponent ai
        if (opponent.y < ball.y) {
            opponent.y += opponent_speed;
        }
        if (opponent.y + paddle_height > ball.y) {
            opponent.y -= opponent_speed;
        }

        if (opponent.y < 0) {
            opponent.y = 0;
        }
        if (opponent.y + paddle_height > win_height) {
            opponent.y = win_height - paddle_height;
        }

        // render
        _ = sdl.SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);

        _ = sdl.SDL_RenderClear(renderer);
        _ = sdl.SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);

        _ = sdl.SDL_RenderFillRect(renderer, &ball);
        _ = sdl.SDL_RenderFillRect(renderer, &player);
        _ = sdl.SDL_RenderFillRect(renderer, &opponent);

        _ = sdl.SDL_RenderDrawLine(renderer, win_width / 2, 0, win_width / 2, win_height);

        _ = sdl.SDL_RenderPresent(renderer);

        std.time.sleep(16 * 1000 * 1000); // sleep for 16ms
    }
}
